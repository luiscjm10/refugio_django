from django.conf.urls import url

from apps.adopcion.views import index_adopcion, hola

urlpatterns = [
    url(r'^$', index_adopcion),
    url(r'^hola$', hola),
]