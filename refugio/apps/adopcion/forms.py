from django import forms

from apps.adopcion.models import Persona, Solicitud



class PersonaForm(forms.ModelForm):

    class Meta:
        model = Persona

        fields = [
            'nombre',
            'apellidos',
            'edad',
            'telefono',
            'email',
            'direccion',
        ]
        labels = {
            'nombre': 'Nombre',
            'apellidos': 'Apellidos',
            'edad': 'Edad',
            'telefono': 'Telefono',
            'email': 'Correo',
            'direccion': 'Direccion',
        }
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'apellidos': forms.TextInput(attrs={'class':'form-control'}),
            'edad': forms.NumberInput(attrs={'class':'form-control'}),
            'telefono': forms.NumberInput(attrs={'class':'form-control'}),
            'email': forms.EmailInput(attrs={'class':'from-control'}),
            'direccion': forms.Textarea(attrs={'class':'from-control'}),
        }



class SolicitudForm(forms.ModelForm):

    class Meta:
        model = Solicitud

        fields = [
            'numero_mascota',
            'razones',
        ]

        labels = {
            'numero_mascotas': 'Numeros de Macotas',
            'razones': 'Motivos de adopcion',
        }

        widgets = {
            'numero_mascota': forms.NumberInput(attrs={'class':'form-control'}),
            'razones': forms.Textarea(attrs={'class': 'form-control'}),
        }