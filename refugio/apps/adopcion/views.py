from django.shortcuts import render
from django.http import HttpResponse #importamos http
from django.views.generic import ListView

from apps.adopcion.models import Persona, Solicitud

# Create your views here.

def index_adopcion(request):
	return HttpResponse("index adopcion")

class SolicitudList(ListView):
	model = Solicitud