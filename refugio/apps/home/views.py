from django.shortcuts import render
from django.http import HttpResponse #importamos http

# Create your views here.
def index(request):
	return render(request, 'home/home_index.html')